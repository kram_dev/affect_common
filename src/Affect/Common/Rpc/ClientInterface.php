<?php

namespace Affect\Common\Rpc;

interface ClientInterface
{
    /**
     * Sends request.
     *
     * @param string $method
     * @param array  $params
     *
     * @return array
     *
     * @throws FaultException
     */
    function request($method, array $params = []);

    /**
     * Returns last request.
     *
     * @return string
     */
    function getLastRequest();

    /**
     * Returns last response.
     *
     * @return string
     */
    function getLastResponse();
}
