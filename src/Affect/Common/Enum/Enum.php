<?php

namespace Affect\Common\Enum;

abstract class Enum implements EnumInterface
{
    protected $value;

    /**
     * Creates a new value of some type.
     *
     * @param mixed $value  Type and default value depends on the extension class.
     * @param bool  $strict Whether to set the object's sctrictness.
     *
     * @throws \UnexpectedValueException if incompatible type is given.
     */
    public function __construct($value, $strict = true)
    {
        if (!in_array($value, static::getConstList(), $strict)) {
            throw new \UnexpectedValueException(sprintf('Unexpected enum value "%s"', $value));
        }

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getReadable();
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $default
     *
     * @return string
     */
    public function getReadable($default = null)
    {
        $map = static::getReadablesMap();

        return isset($map[$this->value]) ? $map[$this->value] : $default;
    }

    /**
     * @return Enum[]
     */
    public static function all()
    {
        $all = [];

        foreach (static::getConstList() as $value) {
            $all[] = new static($value);
        }

        return $all;
    }

    /**
     * @param string $readable
     *
     * @throws \UnexpectedValueException
     *
     * @return EnumInterface
     */
    public static function createFromReadable($readable)
    {
        $map = static::getReadablesMap();

        if (false !== $value = array_search($readable, $map, true)) {
            return new static($value);
        }

        throw new \UnexpectedValueException();
    }

    /**
     * @param EnumInterface $enum
     *
     * @return bool
     */
    public function isEquals(EnumInterface $enum)
    {
        return get_class($this) === get_class($enum)
            && $this->getValue() === $enum->getValue();
    }
}
