<?php

namespace Affect\Common\Enum;

interface EnumInterface
{
    /**
     * Returns all consts (possible values) as an array.
     *
     * @return array
     */
    static function getConstList();

    /**
     * Returns all readables as an array.
     *
     * @return array
     */
    static function getReadablesMap();

    /**
     * @return EnumInterface[]
     */
    static function all();

    /**
     * @param string $readable
     *
     * @throws \UnexpectedValueException
     *
     * @return EnumInterface
     */
    static function createFromReadable($readable);

    /**
     * @return mixed
     */
    function getValue();

    /**
     * @return string
     */
    function getReadable();

    /**
     * @param EnumInterface $enum
     *
     * @return bool
     */
    function isEquals(EnumInterface $enum);
}
