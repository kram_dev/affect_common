<?php

namespace Affect\Common\BrowserKit;

use Symfony\Component\BrowserKit\Request as SymfonyRequest;

class Request extends SymfonyRequest
{
    private $headers;

    /**
     * @param string $uri
     * @param string $method
     * @param array  $headers
     * @param array  $parameters
     * @param array  $files
     * @param array  $cookies
     * @param array  $server
     * @param string $content
     */
    public function __construct($uri, $method, array $headers = [], array $parameters = array(), array $files = array(), array $cookies = array(), array $server = array(), $content = null)
    {
        parent::__construct($uri, $method, $parameters, $files, $cookies, $server, $content);

        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }
}
