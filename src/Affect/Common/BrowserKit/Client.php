<?php

namespace Affect\Common\BrowserKit;

use Symfony\Component\BrowserKit\Response;

class Client implements ClientInterface
{
    const DEFAULT_TIMEOUT = 15;
    const USER_AGENT      = 'Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14';

    /**
     * @var array Additional curl options.
     */
    private $options;

    /** @var string */
    private $unparsedHeaders;

    /**
     * @var array Allowed additional curl options.
     */
    private static $allowedCurlOptions = [
        CURLOPT_FOLLOWLOCATION,
        CURLOPT_HTTP_VERSION,
        CURLOPT_MAXREDIRS,
        CURLOPT_SSL_VERIFYPEER,
        CURLOPT_TIMEOUT,
        CURLOPT_USERAGENT,
        CURLOPT_USERPWD,
        CURLOPT_COOKIE,
    ];

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options + $this->getDefaultOptions();
    }

    /**
     * @return array
     */
    public function getDefaultOptions()
    {
        return [
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HEADER          => true,
            CURLINFO_HEADER_OUT     => true,
            CURLOPT_FOLLOWLOCATION  => false,
            CURLOPT_MAXREDIRS       => 5,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_TIMEOUT         => self::DEFAULT_TIMEOUT,
            CURLOPT_USERAGENT       => self::USER_AGENT
        ];
    }

    /**
     * @param string $login
     * @param string $password
     */
    public function setAuth($login, $password)
    {
        $this->setOption(CURLOPT_USERPWD, $login . ':' . $password);
    }

    public function setCookies($cookies = array())
    {
        $cookiesLine = "";
        foreach ($cookies as $name => $value) {
            $cookiesLine .= ("" == $cookiesLine) ? "{$name}={$value}" : ";{$name}={$value}";
        }

        $this->setOption(CURLOPT_COOKIE, $cookiesLine);
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @throws ClientException
     */
    public function setOption($name, $value)
    {
        if (!in_array($name, self::$allowedCurlOptions, true)) {
            throw new ClientException('Cannot set client option as it is not allowed.');
        }

        $this->options[$name] = $value;
    }

    /**
     * Makes a request.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ClientException
     */
    public function request(Request $request)
    {
        $handler = curl_init();

        if (!curl_setopt_array($handler, $this->options)) {
            throw new ClientException('Unable to set all CURL options.');
        }

        curl_setopt($handler, CURLOPT_HTTPHEADER, $this->buildHeadersFromRequest($request));
        $this->unparsedHeaders = '';
        curl_setopt($handler, CURLOPT_HEADERFUNCTION, array($this, 'headerCallback'));

        $uri= null;

        if ('POST' === $method = strtoupper($request->getMethod())) {
            $uri = $request->getUri();
            curl_setopt($handler, CURLOPT_POST, true);

            $params = $request->getParameters();
            $content = $request->getContent();

            if (count($params)) {
                curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($params));
            } elseif (!empty($content)) {
                curl_setopt($handler, CURLOPT_POSTFIELDS, $content);
            }
        } elseif ('GET' === $method) {
            $uri = $request->getUri();

            if (count($request->getParameters()) > 0) {
                if (null === $query = parse_url($uri, PHP_URL_QUERY)) {
                    $uri .= '?';
                } else {
                    $uri .= '&';
                }

                $uri .= http_build_query($request->getParameters());
            }
        }

        curl_setopt($handler, CURLOPT_URL, $uri);
        $result = curl_exec($handler);

        if (false === $result) {
            throw new ClientException(curl_error($handler), curl_errno($handler));
        }

        $headerSize = strlen($this->unparsedHeaders);
        $headers = $this->unparsedHeaders;
        $content = substr($result, $headerSize);
        /*
         * Альтернативный вариант:
         * $headerSize = curl_getinfo($handler, CURLINFO_HEADER_SIZE);
         * $headers = substr($result, 0, $headerSize);
         * $content = substr($result, $headerSize);
         */

        $response = new Response(
            $content,
            curl_getinfo($handler, CURLINFO_HTTP_CODE),
            $this->buildHeadersFromString($headers)
        );

        curl_close($handler);

        return $response;
    }

    /**
     * @param resource $handler
     * @param string $headerLine
     * @return int
     */
    private function headerCallback($handler, $headerLine)
    {
        $this->unparsedHeaders .= $headerLine;
        return strlen($headerLine);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function buildHeadersFromRequest(Request $request)
    {
        $headers = [];

        foreach ($request->getHeaders() as $key => $value) {
            $headers[] = $key . ': ' . $value;
        }

        return $headers;
    }

    /**
     * @param string $headers
     *
     * @return array
     */
    private function buildHeadersFromString($headers)
    {
        $out = [];
        $headers = explode("\n", trim($headers));

        foreach ($headers as $header) {
            $parsedHeader = explode(':', $header, 2);
            if (isset($parsedHeader[1])) { // отфильтруем пустые строки и строки с HTTP-кодами
                list ($key, $value) = $parsedHeader;
                $key = trim($key);
                $value = trim($value);
                $out[$key] = isset($out[$key]) ? $out[$key] . '; ' : '';
                $out[$key] .= $value;
            }
        }

        return $out;
    }
}
