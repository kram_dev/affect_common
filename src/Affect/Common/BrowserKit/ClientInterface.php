<?php

namespace Affect\Common\BrowserKit;

use Symfony\Component\BrowserKit\Response;

interface ClientInterface
{
    /**
     * Makes a request.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ClientException
     */
    function request(Request $request);

    /**
     * @param mixed $name
     * @param mixed $value
     */
    function setOption($name, $value);
}
