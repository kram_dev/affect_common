<?php

namespace Affect\Common\Json;

use Affect\Common\BrowserKit\Client as BrowserKitClient;
use Affect\Common\BrowserKit\ClientException;
use Affect\Common\BrowserKit\ClientInterface as BrowserKitClientInterface;
use Affect\Common\BrowserKit\Request;
use Affect\Common\Rpc\ClientInterface;
use Affect\Common\Rpc\FaultException;

class Client implements ClientInterface
{
    const VERSION_1                = '1.0';
    const VERSION_2                = '2.0';
    const DEFAULT_USERAGENT_FORMAT = '%name% (%environment%)';

    private $options;
    private $client;
    private $lastRequest;
    private $lastResponse;
    private $classMap;

    /**
     * Constructor.
     *
     * @param string                    $uri
     * @param array                     $options
     * @param BrowserKitClientInterface $client
     */
    public function __construct($uri, array $options = [], BrowserKitClientInterface $client = null)
    {
        $this->uri = $uri;
        $this->setOptions($options);
        $this->client = $client ?: new BrowserKitClient();

        if (isset($this->options['auth'])) {
            $this->client->setAuth($this->options['auth']['username'], $this->options['auth']['password']);
        }
        if (isset($this->options['app'])) {
            $format = isset($this->options['app']['format'])
                ? $this->options['app']['format']
                : self::DEFAULT_USERAGENT_FORMAT;
            $useragent = str_replace(
                ['%name%', '%environment%'],
                [$this->options['app']['name'], $this->options['app']['environment']],
                $format
            );
            $this->client->setOption(CURLOPT_USERAGENT, $useragent);
        }
    }

    /**
     * Sends request.
     *
     * @param string $method
     * @param array  $params
     *
     * @return array
     *
     * @throws FaultException
     */
    public function request($method, array $params = [])
    {
        $uniqId = uniqid();
        $this->lastRequest = json_encode([
            'jsonrpc' => $this->options['version'],
            'method'  => $method,
            'params'  => $params,
            'id'      => $uniqId
        ]);

        $request = new Request($this->uri, 'POST', ['Content-Type' => 'application/json-rpc'], [], [], [], [], $this->lastRequest);

        try {
            $response = $this->client->request($request);
            $this->lastResponse = $response->getContent();

            if (200 !== $code = $response->getStatus()) {
                throw new FaultException(sprintf('Bad HTTP response code "%s"', $code));
            }

            if (null === $decoded = json_decode($this->lastResponse, true)) {
                throw new FaultException('Mailformed json response: ' . $this->lastResponse);
            }

            if ($decoded['id'] !== $uniqId) {
                throw new FaultException('Response id mismatch.');
            }

            if (isset($decoded['error'])) {
                throw new FaultException($decoded['error']['message'], $decoded['error']['code']);
            }

            $className = isset($this->classMap[$method]) ? $this->classMap[$method] : null;

            return null === $className ? $decoded['result'] : new $className($decoded['result']);
        } catch (ClientException $e) {
            throw new FaultException(sprintf('Unable to perform RPC request <%s:%s>', $e->getCode(), $e->getMessage()), 0, $e);
        }
    }

    /**
     * Returns last request.
     *
     * @return string
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    /**
     * Returns last response.
     *
     * @return string
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * Sets the options values.
     *
     * @param array $options
     */
    private function setOptions(array $options)
    {
        $this->options = ['version' => self::VERSION_2];

        if (isset($options['version'])) {
            $this->options['version'] = $options['version'];
            unset($options['version']);
        }

        if (isset($options['auth']['username']) && isset($options['auth']['password'])) {
            $this->options['auth'] = $options['auth'];
            unset($options['auth']);
        }

        if (isset($options['app']['name']) && isset($options['app']['environment'])) {
            $this->options['app'] = $options['app'];
            unset($options['app']);
        }

        if (isset($options['class_map']) && is_array($options['class_map']) && 0 < count($options['class_map'])) {
            $this->classMap = $options['class_map'];
            unset($options['class_map']);
        }
    }
}
