<?php

namespace Affect\Common\Json;

use Psr\Log\LoggerInterface;
use Affect\Common\Server\FaultMap;
use Zend\Json\Server\Error;
use Zend\Json\Server\Exception\InvalidArgumentException;
use Zend\Json\Server\Request;
use Zend\Json\Server\Response;
use Zend\Json\Server\Server as ZendServer;

class Server extends ZendServer
{
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var FaultMap
     */
    protected $faultMap;

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param FaultMap $map
     */
    public function registerFaultMap(FaultMap $map)
    {
        $this->faultMap = $map;
    }

    /**
     * Handle request
     *
     * @param Request|bool $request
     *
     * @return Response|null
     *
     * @throws InvalidArgumentException
     */
    public function handle($request = false)
    {
        if ((false !== $request) && (!$request instanceof Request)) {
            throw new InvalidArgumentException('Invalid request type provided; cannot handle');
        } elseif ($request) {
            $this->setRequest($request);
        }

        // Handle request
        $this->_handle();

        // Get response
        $response = $this->_getReadyResponse();

        null !== $this->logger && $this->logger->debug('RESPONSE: ' . (string) $response);

        // Emit response?
        if (!$this->returnResponse) {
            echo $response;
            return;
        }

        // or return it?
        return $response;
    }

    /**
     * Internal method for handling request
     *
     * @return void
     */
    protected function _handle()
    {
        $request = $this->getRequest();

        null !== $this->logger && $this->logger->debug('REQUEST: ' . (string) $request);

        if (!$request->isMethodError() && (null === $request->getMethod())) {
            null !== $this->logger && $this->logger->alert('FAULT [' . -32600 . '] Method not defined');

            return $this->fault('Invalid Request', Error::ERROR_INVALID_REQUEST);
        }

        if ($request->isMethodError()) {
            null !== $this->logger && $this->logger->alert('FAULT [' . -32600 . '] Invalid method name');

            return $this->fault('Invalid Request', Error::ERROR_INVALID_REQUEST);
        }

        $method = $request->getMethod();
        if (!$this->table->hasMethod($method)) {
            null !== $this->logger && $this->logger->alert('FAULT [' . -32601 . '] Method "' . $method . '" not found');

            return $this->fault('Method not found', Error::ERROR_INVALID_METHOD);
        }

        $params        = $request->getParams();
        $invocable     = $this->table->getMethod($method);
        $serviceMap    = $this->getServiceMap();
        $service       = $serviceMap->getService($method);
        $serviceParams = $service->getParams();

        if (count($params) < count($serviceParams)) {
            $params = $this->_getDefaultParams($params, $serviceParams);
        }

        //Make sure named parameters are passed in correct order
        if (is_string( key( $params ) )) {

            $callback = $invocable->getCallback();
            if ('function' == $callback->getType()) {
                $reflection = new \ReflectionFunction( $callback->getFunction() );
            } else {

                $reflection = new \ReflectionMethod(
                    $callback->getClass(),
                    $callback->getMethod()
                );
            }

            $orderedParams = array();
            foreach ($reflection->getParameters() as $refParam) {
                if (isset( $params[ $refParam->getName() ] )) {
                    $orderedParams[ $refParam->getName() ] = $params[ $refParam->getName() ];
                } elseif ($refParam->isOptional()) {
                    $orderedParams[ $refParam->getName() ] = null;
                } else {
                    null !== $this->logger && $this->logger->alert('FAULT: Missing required parameter "' . $refParam->getName() .'"');

                    return $this->fault('Invalid params', Error::ERROR_INVALID_PARAMS);
                }
            }
            $params = $orderedParams;
        }

        try {
            $result = $this->_dispatch($invocable, $params);
        } catch (\Exception $e) {
            null !== $this->logger && $this->logger->alert('FAULT [' . $e->getCode() . ']: ' . $e->getMessage());

            return $this->fault(
                $e->getMessage(),
                null === $this->faultMap ? $e->getCode() : $this->faultMap->getCode($e),
                $e
            );
        }

        $this->getResponse()->setResult($result);
    }
}
