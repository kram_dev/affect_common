<?php

namespace Affect\Common\Soap;

use Psr\Log\LoggerInterface;

class Client extends \SoapClient
{
    protected $wsdl;
    protected $options;
    protected $logger;
    protected $inited;
    protected $headers;

    /**
     * Constructor.
     *
     * @param string $wsdl
     * @param array $options
     * @param LoggerInterface $logger
     */
    public function __construct($wsdl = "", array $options = [], LoggerInterface $logger = null)
    {
        $this->wsdl    = $wsdl;
        $this->options = $options;
        $this->logger  = $logger;
        $this->inited  = false;
        $this->headers = [];
    }

    /**
     * Adds additional headers to client.
     *
     * @param \SoapHeader $header
     *
     * @throws \LogicException
     */
    public function addHeader(\SoapHeader $header)
    {
        if ($this->inited) {
            throw new \LogicException('Unable to add SoapHeader to already inited client.');
        }

        $this->headers[] = $header;
    }

    /**
     * Set headers to client.
     * All previously setted headers will be deleted.
     *
     * @param \SoapHeader[] $headers
     */
    public function setHeaders(array $headers)
    {
        $this->__setSoapHeaders();
        $this->__setSoapHeaders($headers);
    }

    /**
     * Init SoapClient.
     */
    public function init()
    {
        if (!$this->inited) {
            $this->SoapClient($this->wsdl, $this->options);
            $this->__setSoapHeaders($this->headers);
            $this->inited = true;
        }
    }

    /**
     * Calls a SOAP function.
     * Этот метод согласно документации php - deprecated
     * Лучше плавно переходить на __soapCall, который ниже уже переопределен.
     *
     * @param string $function_name
     * @param array $arguments
     *
     * @throws \Exception|\SoapFault
     * @return mixed
     */
    public function __call($function_name, $arguments)
    {

        $this->init();

//        if (null !== $this->logger) {
//            $this->logger->debug('METHOD: ' . $function_name . ', PARAMS: ' . json_encode($arguments));
//            // @TODO json_encode не покажет закрытые свойства классов, поэтому в данном случае нужен иной подход
//            // или же воспользоваться советами
//            // в http://stackoverflow.com/questions/4697656/using-json-encode-on-objects-in-php-regardless-of-scope
//        }

        try {
            $result = parent::__call($function_name, $arguments[0]);

            if (null !== $this->logger) {
                $this->logger->debug('REQUEST: ' . $this->__getLastRequest());
                $this->logger->debug('RESPONSE: ' . $this->__getLastResponse());
            }
        } catch (\SoapFault $e) {
            if (null !== $this->logger) {
                $this->logger->debug('REQUEST: ' . $this->__getLastRequest());
                $this->logger->debug('FAULT: ' . $this->__getLastResponse());
            }

            throw $e;
        }

        return $result;
    }

    /**
     * Переопределим, потому что нам нужно логирование.
     *
     * @param string $function_name
     * @param array $arguments
     * @param null $options
     * @param null $input_headers
     * @param null $output_headers
     *
     * @return mixed
     * @throws \Exception
     * @throws \SoapFault
     */
    public function __soapCall(
        $function_name,
        $arguments,
        $options = null,
        $input_headers = null,
        &$output_headers = null
    ) {
        $this->init();

        if (null !== $this->logger) {
            $stopList = ['setAvatar', 'setPassport']; //эти методы отправляют бинарные файлы на них падает json_encode
            if (!in_array($function_name, $stopList)) {
                $this->logger->debug('METHOD: ' . $function_name . ', PARAMS: ' . json_encode($arguments));
            // @TODO json_encode не покажет закрытые свойства классов, поэтому в данном случае нужен иной подход
            // или же воспользоваться советами
            // в http://stackoverflow.com/questions/4697656/using-json-encode-on-objects-in-php-regardless-of-scope
            }else{
                $this->logger->debug('METHOD: ' . $function_name . ', PARAMS: has binary data');
            }
        }

        try {
            $result = parent::__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);

            if (null !== $this->logger) {
                $this->logger->debug('REQUEST: ' . $this->__getLastRequest());
                $this->logger->debug('RESPONSE: ' . $this->__getLastResponse());
            }
        } catch (\SoapFault $e) {
            if (null !== $this->logger) {
                $this->logger->debug('REQUEST: ' . $this->__getLastRequest());
                $this->logger->debug('FAULT: ' . $this->__getLastResponse());
            }

            throw $e;
        }

        return $result;
    }
}
