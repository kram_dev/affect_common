<?php

namespace Affect\Common\Server;

class FaultMap
{
    private $map;

    /**
     * @param array $map
     */
    public function __construct(array $map = [])
    {
        foreach ($map as $code => $className) {
            $this->addCode($code, $className);
        }
    }

    /**
     * @param int    $code
     * @param string $className
     *
     * @throws \InvalidArgumentException
     */
    public function addCode($code, $className)
    {
        if (!is_int($code)) {
            throw new \InvalidArgumentException('Invalid fault code, expected integer value, but got "' . gettype($code) . '"');
        }

        if (isset($this->map[$code])) {
            throw new \InvalidArgumentException('Fault code is already exists.');
        }

        $this->map[$code] = $className;
    }

    /**
     * @param \Exception $e
     *
     * @return int|null
     */
    public function getCode(\Exception $e)
    {
        return array_search(get_class($e), $this->map, true) ?: null;
    }

    /**
     * @param int $code
     *
     * @return string|null
     */
    public function getClassName($code)
    {
        return isset($this->map[$code]) ? $this->map[$code] : null;
    }
}
