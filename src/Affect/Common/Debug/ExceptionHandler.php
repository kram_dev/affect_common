<?php

namespace Affect\Common\Debug;

use Psr\Log\LoggerInterface;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use Symfony\Component\HttpFoundation\Response;

class ExceptionHandler extends SymfonyExceptionHandler
{
    /**
     * @var LoggerInterface
     */
    private static $logger;

    /**
     * @param LoggerInterface $logger
     */
    public static function setLogger(LoggerInterface $logger)
    {
        self::$logger = $logger;
    }

    /**
     * Sends a Response for the given Exception.
     *
     * @param \Exception $exception An \Exception instance
     */
    public function handle(\Exception $exception)
    {
        if (null !== self::$logger) {
            $createMessage = function(\Exception $exception) {
                return '[' . $exception->getCode() . '] ' . $exception->getMessage()
                    . ' (' . get_class($exception) . ')' . PHP_EOL
                    . $exception->getTraceAsString();
            };

            $message = $createMessage($exception);

            while (null !== $previous = $exception->getPrevious()) {
                $exception = $previous;
                $message .= PHP_EOL . PHP_EOL . $createMessage($exception);
            }

            self::$logger->alert($message);
        }

        (new Response('', 500))->send();
    }
}
