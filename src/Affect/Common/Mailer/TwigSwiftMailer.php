<?php

namespace Affect\Common\Mailer;

use Swift_Attachment;
use Swift_Image;
use Symfony\Component\Config\Definition\Exception\Exception;

class TwigSwiftMailer
{
    /** @var \Swift_Mailer */
    protected $mailer;

    /** @var \Twig_Environment */
    protected $twig;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig   = $twig;
    }

    /**
     * Render a template and send result as email
     *
     * @param string $templateName
     * @param array $context
     * @param string|array $from
     * @param string|array $to
     *
     * @return int
     */
    public function sendEmail($templateName, $context, $from, $to)
    {
        $message = \Swift_Message::newInstance();

        if (isset($context['attachments'])) {
            $attachments = $context['attachments'];

            if (!is_array($attachments)) {
                $attachments = [$attachments];
            }

            foreach ($attachments as $fileName => $filePathName) {
                if (!is_int($fileName)) {
                    $attach = Swift_Attachment::fromPath($filePathName)->setFilename($fileName);
                } else {
                    $attach = Swift_Attachment::fromPath($filePathName);
                }

                $message->attach($attach);
            }
        }

        //вставим встроенные файлы...
        if (isset($context['inline'])) {
            $attachments = $context['inline'];

            if (is_array($attachments)) {
                foreach ($attachments as $fileName => $filePathName) {
                    $attachCid = $message->embed(Swift_Image::fromPath($filePathName), 'image/jpeg');
//--for swiftmail 5.3
//                    $attach  = Swift_Attachment::fromPath($filePathName, 'image/jpeg')
//                        ->setFilename( 'wow-poster.jpg')
//                        ->setDisposition('inline')
//                    ;
//                    $attachCid          = $message->embed($attach);
//---/
                    $context[$fileName] = $attachCid;
                }
            }
        }

        $context = $this->twig->mergeGlobals($context);
        /** @var \Twig_Template $template */
        $template = $this->twig->loadTemplate($templateName);

        $subject  = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message->setSubject($subject)
            ->setFrom($from)
            ->setTo($to);

         if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $failedRecipients = [];
        //Swift_TransportException
        try{
            $res = $this->mailer->send($message, $failedRecipients);
        }catch (Exception $e){
             //todo пишем в лог... но оттуда всёравно ничего не приходит
             var_dump($e);
             exit;
        }
    }
}
