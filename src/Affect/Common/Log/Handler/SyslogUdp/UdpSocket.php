<?php

/**
 * в данном классе, в отличии от оригинального (monolog), многострочные сообщения не режутся на строки,
 * а просто усекаются до заданной через конфиг длины log_datagram_max_length
 */

namespace Affect\Common\Log\Handler\SyslogUdp;

use Monolog\Handler\SyslogUdp\UdpSocket as MonologUdpSocket;

class UdpSocket extends  MonologUdpSocket
{

    /**
     * @param string    $ip
     * @param int       $port
     * @param int       $datagramMaxLength
     */
    public function __construct($ip, $port = 514, $datagramMaxLength = 1400)
    {
        parent::__construct($ip, $port);
        $this->datagramMaxLength = $datagramMaxLength;
    }

    /**
     * @param string    $line
     * @param string    $header
     */
    public function write($line, $header = "")
    {
        $chunkSize = $this->datagramMaxLength - strlen($header);
        $chunk = $header . substr($line, 0, $chunkSize);
        $this->send($chunk);
    }
}
