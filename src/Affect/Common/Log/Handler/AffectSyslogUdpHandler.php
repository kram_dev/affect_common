<?php

namespace Affect\Common\Log\Handler;

use Monolog\Handler\SyslogUdpHandler;
use Affect\Common\Log\Handler\SyslogUdp\UdpSocket;
use Symfony\Bridge\Monolog\Logger;

class AffectSyslogUdpHandler extends SyslogUdpHandler
{
    /** @var string */
    private $host;
    /** @var int    */
    private $port;
    /** @var string */
    private $appName;

    /**
     * @param string    $appName
     * @param string    $host
     * @param int       $port
     * @param mixed     $facility
     * @param bool|int  $level The minimum logging level at which this handler will be triggered
     * @param Boolean   $bubble Whether the messages that are handled can bubble up the stack or not
     * @param int       $datagramMaxLength
     */
    public function __construct(
        $appName = 'APPNAME',
        $host,
        $port = 514,
        $facility = LOG_USER,
        $level = Logger::DEBUG,
        $bubble = true,
        $datagramMaxLength = 1400
    ) {
        parent::__construct($host, $port, $facility, $level, $bubble);

        $this->socket = new UdpSocket($host, $port ?: 514, $datagramMaxLength);
        $this->host  = $host;
        $this->appName = $appName;
        $this->port = $port;
    }

    /**
     * @param array $record
     */
    protected function write(array $record)
    {
        $header = $this->makeCommonSyslogHeader($this->logLevels[$record['level']]);
        $logMessage = $record['formatted'];
        $this->socket->write($logMessage, $header);
    }


    /**
     * Make common syslog header (see rfc5424)
     * отличиет от стандартного- добавлено APPNAME
     */
    private function makeCommonSyslogHeader($severity)
    {
        $priority = $severity + $this->facility;

        return "<$priority>{$this->appName}:";
    }
}
