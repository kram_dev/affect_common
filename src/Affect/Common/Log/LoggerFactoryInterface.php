<?php

namespace Affect\Common\Log;

use Psr\Log\LoggerInterface;

interface LoggerFactoryInterface
{
    /**
     * Creates logger instance.
     *
     * @param string $name
     *
     * @return LoggerInterface
     */
    function create($name);
}
