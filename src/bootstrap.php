<?php
error_reporting(E_ALL | E_STRICT);

// register silently failing autoloader
spl_autoload_register(function($class) {
    if (0 === strpos($class, 'Affect\Common\\')) {
        $path = __DIR__.'/'.strtr($class, '\\', '/').'.php';

        if (file_exists($path) && is_readable($path)) {
            require_once $path;

            return true;
        }
    }
});